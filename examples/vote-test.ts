import { BITBOX } from "bitbox-sdk";
import { ECPair } from "bitcoincashjs-lib";
import { Contract, SignatureTemplate } from "cashscript";
import log from "npmlog";
import { Blockchain } from "../lib/blockchain";
import { BLANK_VOTE, DUST } from "../lib/votedef";
import { calc_proposal_id, create_salt,
    create_two_option_contract, hash160, hash160_salted, sha256,
    create_vote_message, sign_vote_message, get_txid } from "../lib/voteutil";
import { tally_votes } from "../lib/voteverify";
import { instantiateSha256, decodePrivateKeyWif } from "@bitauth/libauth";
const util = require("util");

const sleep = require("sleep");
const assert = require("assert");

const TX_FEE_GUESS = 450;

// Change address can be anything.
const CHANGE_ADDRESS = "bitcoincash:qr8p3mpz2gm9ykhj0k50fhtrxnfc87qs8yf7l3slzv";

// TX_FEE_GUESS + mininal for final output
const MIN_BALANCE = TX_FEE_GUESS + DUST;

run().catch((err) => {
    log.warn("vote", err);
});

function to_pk(bitbox: BITBOX, voter: any) {
    return bitbox.ECPair.toPublicKey(voter);
}

async function to_pkh(bitbox: BITBOX, voter: any): Promise<Buffer> {
    return await hash160(to_pk(bitbox, voter));
}

function to_addr(bitbox: BITBOX, voter: any) {
    return bitbox.ECPair.toCashAddress(voter);
}

export function log_tally(result: any) {
    log.info("result", "Option A: %s", result[0].map((x: any) => {
        return util.format("[User %s in tx %s]", x[1].toString("hex"), x[0].getId());
    }));
    log.info("result", "Option B: %s", result[1].map((x: any) => {
        return util.format("[User %s in tx %s]", x[1].toString("hex"), x[0].getId());
    }));
    log.info("result", "Blank: %s", result[2].map((x: any) => {
        return util.format("[User %s in tx %s]", x[1].toString("hex"), x[0].getId());
    }));
    log.info("result", "Did not vote: ", result[3].map((x: Buffer) => {
        return x.toString("hex");
    }));
    log.info("result", "Had invalid votes: ", result[4].map((x: Buffer) => {
        return x.toString("hex");
    }));
}

export async function run(): Promise<void> {
    const network = "mainnet";
    const bitbox = new BITBOX({ restURL: "https://rest.bitcoin.com/v2/" });

    const rootSeed = bitbox.Mnemonic.toSeed("votercash");
    const hdNode = bitbox.HDNode.fromSeed(rootSeed, network);

    const admin = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 0));
    const voter1: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 1));
    const voter2: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 2));
    const voter3: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 3));
    const voter4: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 4));
    const voter5: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 5));
    const voter6: ECPair = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 6));

    const salt = create_salt();

    const description = "Foo?";
    const optA = "Bar";
    const optAHash = await hash160_salted(salt, Buffer.from(optA));
    const optB = "Baz";
    const optBHash = await hash160_salted(salt, Buffer.from(optB));

    const voters: ECPair[] = [voter1, voter2, voter3, voter4, voter5, voter6];
    const votersPKH: Buffer[] = [];
    for (const v of voters) {
        votersPKH.push(await to_pkh(bitbox, v));
    }

    const chain = new Blockchain();
    await chain.connect();

    // Set end height to the next block
    const endheight = 1 + await chain.get_blockchain_tip_height();

    const id = await calc_proposal_id(salt, description, optA, optB, endheight, votersPKH);

    const contracts: [ECPair, Contract][] = [];
    for (const voter of voters) {
        const contract = create_two_option_contract(network, id,
            optAHash, optBHash, await to_pkh(bitbox, voter));

        contracts.push([voter, contract]);
    }

    log.info("vote", "Using salt", salt.toString("hex"));
    log.info("vote", "End height %d", endheight);
    log.info("vote", "Voting on proposal with ID: %s", id.toString("hex"));
    log.info("vote", "Option A: %s (%s)", optA, optAHash.toString("hex"));
    log.info("vote", "Option B: %s (%s)", optB, optBHash.toString("hex"));
    log.info("vote", "Blank vote: %s", BLANK_VOTE.toString("hex"));

    const castVote = async (voter: ECPair, contract: Contract, option: Buffer, changeaddr: string) => {
        log.info("vote", "Voter %s to vote for %s",
            (await to_pkh(bitbox, voter)).toString("hex"), option.toString("hex"));
        log.info("vote", "Contract address: %s", contract.address);

        let contractBalance = await contract.getBalance();
        while (contractBalance < MIN_BALANCE) {
            log.info("vote", "Too low balance (%d < %d), waiting...",
                contractBalance, MIN_BALANCE);

            contractBalance = await contract.getBalance();
            sleep.sleep(5);
        }
        assert(contractBalance >= DUST + TX_FEE_GUESS);

        log.info("vote", "Fee %s, change %s to %s", TX_FEE_GUESS,
            contractBalance - TX_FEE_GUESS, changeaddr);

        const message = create_vote_message(id, option)
        const sha256instance = await instantiateSha256()
        const privkey: any = decodePrivateKeyWif(sha256instance, voter.toWIF())
        const messageSig = await sign_vote_message(privkey.privateKey, message)

        const pubkey: Buffer = await to_pk(bitbox, voter)
        const tx = await contract.functions.cast_vote(
            pubkey, new SignatureTemplate(voter), message, messageSig)
            .to(changeaddr, contractBalance - TX_FEE_GUESS)
            .withHardcodedFee(TX_FEE_GUESS)
            .send()
            .catch((e: any) => {
                    throw e;
            });

        log.info("vote", "Success %s!", await get_txid(tx))
    };

    assert(contracts.length === 6);

    // Votes Option A
    await castVote(contracts[0][0], contracts[0][1], optAHash, contracts[1][1].address);

    // Votes for option B
    await castVote(contracts[1][0], contracts[1][1], optBHash, contracts[2][1].address);

    // Two blank votes
    await castVote(contracts[2][0], contracts[2][1], BLANK_VOTE, contracts[3][1].address);
    await castVote(contracts[3][0], contracts[3][1], BLANK_VOTE, contracts[4][1].address);

    // This voter invalidates his vote by voting twice
    await castVote(contracts[4][0], contracts[4][1], optAHash, contracts[4][1].address);
    await castVote(contracts[4][0], contracts[4][1], optBHash, CHANGE_ADDRESS);

    // Finally, the last voter (contracts[5]) does not vote ...

    log.info("result", "Waiting for transactions to propagate...");
    sleep.sleep(10);
    try {
        const result = await tally_votes(chain, id, optAHash, optBHash, votersPKH, endheight, true);
        log_tally(result);
    } finally {
        chain.disconnect();
    }
}
