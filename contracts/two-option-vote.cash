pragma cashscript ^0.5.0;

/**
 * Two-option-vote contract v1.1
 *
 * Contract for casting a vote on the blockchain. The contract supports
 * two options + blank vote.
 *
 * @param voter_pkh Hash160 of voters public key.
 * @param option_a Hash160 of the first option.
 * @param option_b Hash160 of the second option.
 * @param proposal Hash160 representing the proposal voted on.
 */
contract TwoOptionVote(
    bytes20 voter_pkh,
    bytes20 option_a, bytes20 option_b,
    bytes20 proposal) {

    /**
     * Cast vote
     *
     * @param pk The voters public key.
     * @param sig Transaction signature.
     * @param msg Proposal ID and option voted on, concatenated.
     * @param msg_sig Message signature.
     */
    function cast_vote(
        pubkey pk, sig tx_sig,
        bytes40 msg, datasig msg_sig)
    {
        require(hash160(pk) == voter_pkh);
        require(checkSig(tx_sig, pk));

        // As the input scriptSig is not and can not be covered by the above
        // checkSig, we use checkDataSig to assure the input has not been
        // tempered with.
        require(checkDataSig(msg_sig, msg, pk));

        require(msg.split(20)[0] == proposal);
        bytes vote = msg.split(20)[1];
        require(
            vote == option_a
            || vote == option_b
            || vote == 0xBEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF /* blank */);
    }
}
