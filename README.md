# Two-option-vote

**[Project documentation](https://dagurval.gitlab.io/two-option-vote/)**

A Bitcoin Cash smart contract and protocol for transparent on-chain voting.

"Two option vote" is a minimalistic and transparent voting protocol on top of
Bitcoin Cash and similar blockchains. Participating, verifying and tallying
does not require a full node, but is fully SPV client compatible. Votes are
non-transferable.

This repository hosts the protocol/smart contract specification and a reference
implementation in javascript.

- **[READ THE SPECIFICATION](two-option-vote-contract)**
- **[READ THE REFERENCE](https://bitcoinunlimited.gitlab.io/two-option-vote/reference/)**

## Build project

`npm run build`

## Examples

See [examples](https://gitlab.com/bitcoinunlimited/two-option-vote/-/tree/master/examples)
