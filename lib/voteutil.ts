import { CashCompiler, Contract, ElectrumNetworkProvider } from "cashscript";
import * as path from "path";
import { OP_DATA_20 } from "./script";
import { CashAddressType, CashAddressNetworkPrefix, encodeCashAddress, instantiateSha256, instantiateRipemd160, instantiateSecp256k1, decodeCashAddress, Transaction, encodeTransaction } from '@bitauth/libauth';
const crypto = require("crypto");
const fs = require("fs");
const assert = require("assert")
const bufferReverse = require('buffer-reverse')

export function create_salt(): Buffer {
    return crypto.randomBytes(20);
}

export async function hash160(buffer: Buffer): Promise<Buffer> {
    const sha = await sha256(buffer)
    const ripemd160 = await instantiateRipemd160()
    const state1 = ripemd160.init()
    const state2 = ripemd160.update(state1, sha)
    return new Buffer(ripemd160.final(state2))
}

export async function sha256(buffer: Buffer): Promise<Buffer> {
    const libsha256 = await instantiateSha256()
    const state1 = libsha256.init()
    const state2 = libsha256.update(state1, buffer)
    return new Buffer(libsha256.final(state2))
}

export async function sha256d(buffer: Buffer): Promise<Buffer> {
    const roundOne = await sha256(buffer)
    const roundTwo = await sha256(new Buffer(roundOne))
    return roundTwo
}

export async function hash160_salted(salt: Buffer, buffer: Buffer): Promise<Buffer> {
    return new Buffer(await hash160(Buffer.concat([salt, buffer])));
}

export function hash_to_address(hash: Uint8Array, addrtype: string): string {
    if (addrtype !== "p2sh" && addrtype !== "p2pkh") {
        throw new Error("Unsupported address type");
    }

    let type = CashAddressType.P2PKH
    if (addrtype === "p2sh") {
        type = CashAddressType.P2SH
    }
    const prefix = CashAddressNetworkPrefix.mainnet
    return encodeCashAddress(prefix, type, hash)
}

export async function calc_proposal_id(
    salt: Buffer,
    description: string,
    optA: string,
    optB: string,
    endheight: number,
    participantsPKH: Buffer[],
): Promise<Buffer> {

    if (!(endheight >= 0)) {
        throw Error("Invalid endheigt");
    }

    // use concat to take a copy
    const participants = participantsPKH.concat().sort(Buffer.compare);

    const intToBuff = (n: number) => {
        const b = Buffer.alloc(4);
        b.writeUInt32BE(n, 0);
        return b;
    };

    const proposalIDParts: Buffer[] = [
        salt,
        Buffer.from(description),
        Buffer.from(optA),
        Buffer.from(optB),
        intToBuff(endheight),
        Buffer.concat(participants),
    ];

    const blob: Buffer = Buffer.concat(proposalIDParts);
    return new Buffer(await hash160(blob))
}

// Find the cashaddr of a vote contract
export async function derive_contract_adr(
    proposalID: Buffer, optA: Buffer, optB: Buffer, voterPKH: Buffer): Promise<string> {

    const hash = await hash160(two_option_contract_redeemscript(
        proposalID, optA, optB, voterPKH));

    return hash_to_address(hash, "p2sh");
}

export function create_two_option_contract(
    network: "mainnet" | "testnet" | undefined,
    proposalID: Buffer,
    optA: Buffer,
    optB: Buffer,
    voterPKH: Buffer): Contract {
    const VoteContract = CashCompiler.compileFile(
        path.join(__dirname, "../contracts/two-option-vote.cash"));

    const provider = new ElectrumNetworkProvider(network)
    return new Contract(VoteContract,
            [voterPKH, optA, optB, proposalID], provider);

}

export function two_option_contract_redeemscript(
    proposalID: Buffer, optA: Buffer, optB: Buffer, voterPKH: Buffer): Buffer {

    const hex = fs.readFileSync(path.join(__dirname,
        "../contracts/two-option-vote.hex"), "utf8");
    const redeemScript = Buffer.from(hex, "hex");

    return Buffer.concat([
        Buffer.alloc(1, OP_DATA_20), proposalID,
        Buffer.alloc(1, OP_DATA_20), optB,
        Buffer.alloc(1, OP_DATA_20), optA,
        Buffer.alloc(1, OP_DATA_20), voterPKH,
        redeemScript]);
}

export function create_vote_message(proposalID: Buffer, option: Buffer): Buffer {
    assert(proposalID.length === 20)
    assert(option.length === 20)
    return Buffer.concat([proposalID, option])
}

export async function sign_vote_message(privateKey: Uint8Array, message: Buffer): Promise<Uint8Array> {
    assert(message.length === 40)
    const secp256k1 = await instantiateSecp256k1()
    const messageHash = await sha256(message)
    return secp256k1.signMessageHashSchnorr(privateKey, messageHash)
}

export function to_output_script(address: string): Buffer {
    const decoded: any = decodeCashAddress(address);
    if (decoded.type === CashAddressType.P2PKH) {
        return Buffer.concat([
            Buffer.from([0x76]), // OP_DUP
            Buffer.from([0xa9]), // OP_HASH160
            Buffer.from([20]), // OP push 20 bytes
            decoded.hash,
            Buffer.from([0x88]), // OP_REQUALVERIFY
            Buffer.from([0xac])  // OP_CHECKSIG
        ])
    }
    if (decoded.type === CashAddressType.P2SH) {
        return Buffer.concat([
            Buffer.from([0xa9]), // OP_HASH160
            Buffer.from([20]), // OP push 20 bytes
            decoded.hash,
            Buffer.from([0x87]) // OP_EQUAL
        ])
    }
    throw Error("unknown cashaddr");
}

export async function get_txid(tx: Transaction): Promise<string> {
    const encoded = encodeTransaction(tx)
    const hashed = await sha256d(new Buffer(encoded))
    return bufferReverse(hashed).toString('hex')
}
