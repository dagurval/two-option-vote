// The hash160 of a blank vote.
export const BLANK_VOTE: Buffer = Buffer.from("BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "hex");

export const DUST = 546;
