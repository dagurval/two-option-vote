import assert from "assert";
import { ElectrumClient } from "electrum-cash";
import { decodeTransaction } from "@bitauth/libauth";
import { to_output_script } from "./voteutil";

/**
 * We don't do SPV verification (yet?), so this needs to be a server that
 * does not lie to us.
 */
const SERVER_WE_TRUST = "electrs.bitcoinunlimited.info";

export class Blockchain {

    public electrum: any;

    constructor() {
        this.electrum = null;
    }

    public async connect() {
        if (this.electrum !== null) {
            return;
        }

        this.electrum = new ElectrumClient("The best client", "1.4", SERVER_WE_TRUST);
        return this.electrum.connect();
    }

    public disconnect() {
        assert(this.electrum !== null);
        this.electrum.disconnect();
    }

    public is_output_to(output: any, addr: string): boolean {
        const script = to_output_script(addr);
        return output.lockingBytecode.compare(script) === 0;
    }

    public async has_input_from(tx: any, addr: string): Promise<boolean> {
        for (const i of tx.inputs) {
            const txIn: any = await this.get_tx(i.outpointTransactionHash.toString('hex'));
            if (this.is_output_to(txIn.outputs[i.outpointIndex], addr)) {
                return true;
            }
        }
        return false;
    }

    public async get_tx(txid: string) {
        assert(this.electrum !== null);
        const txHex = await this.electrum.request(
            "blockchain.transaction.get", txid);
        if (typeof(txHex) !== "string") {
            throw txHex;
        }
        const tx: Buffer = Buffer.from(txHex, 'hex');

        return decodeTransaction(tx)
    }

    /**
     * Get transactions that _spend_ from address.
     *
     * @param addr - Bitcoin Cash address
     * @param maxheight - Filter out transactions confirmed after this height
     * @param includeUnconfirmed - Include unconfirmed (when maxheight > tip)
     */
    public async get_spending_txs(addr: string, maxheight: number,
                                  includeUnconfirmed: boolean): Promise<any[]> {
        assert(this.electrum !== null);

        const history = await this.electrum.request(
            "blockchain.address.get_history", addr);

        const tipheight = await this.get_blockchain_tip_height();

        const txs: any[] = [ ];
        for (const entry of history) {

            // Check if tx is unconfirmed.
            // * 0 means unconfirmed.

            // * -1 means unconfirmed with unconfirmed parent.
            if (entry.height <= 0) {
                if (!includeUnconfirmed) {
                    continue;
                }
                if (tipheight >= maxheight) {
                    // Above max height. Cannot include unconfirmed txs.
                    continue;
                }
            }
            if (entry.height > maxheight) {
                continue;
            }
            const tx = await this.get_tx(entry.tx_hash);
            if (await this.has_input_from(tx, addr)) {
                txs.push(tx);
            }
        }
        return txs;
    }

    public async get_blockchain_tip_height(): Promise<number> {
        const header = await this.electrum.request("blockchain.headers.subscribe");

        // We don't actually want events on new blocks. We just wanted the tip.
        // So unsubscribe immediately.
        try {
            await this.electrum.request("blockchain.headers.unsubscribe");
        } catch {
             // ignore error
        }

        return header.height;
    }
}
