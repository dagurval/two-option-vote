import { Blockchain } from "./blockchain";
import { InvalidVoteError } from "./error/invalidvoteerror";
import { NoVoteError } from "./error/novoteerror";
import { create_random_addr, get_example_vote } from "./testutil";
import { calc_proposal_id, derive_contract_adr } from "./voteutil";
import { fetch_vote_tx, get_verified_vote, tally_votes } from "./voteverify";
import { BLANK_VOTE } from "./votedef";

test("fetch_vote_no_vote_cast_error", async () => {
    const contractAddr = create_random_addr();

    const chain = new Blockchain();
    await chain.connect();

    const t = async () => {
        await fetch_vote_tx(chain, contractAddr, 1337, true);
    };
    await expect(t()).rejects.toEqual(new NoVoteError());

    chain.disconnect();
});

test("get_verified_vote", async () => {
    jest.setTimeout(30000);
    const chain = new Blockchain();
    await chain.connect();

    const vote = await get_example_vote();

    const proposal = vote.proposal;
    const optA = vote.optA;
    const optB = vote.optB;
    const height = vote.endheight;

    const votes = [ ];
    for (const voterpkh of vote.voters.slice(0, 3)) {
        const addr = await derive_contract_adr(proposal, optA, optB, voterpkh);
        const tx = await fetch_vote_tx(chain, addr, height, true);

        votes.push(await get_verified_vote(tx, proposal, optA, optB, voterpkh));
    }
    expect(votes).toEqual([optA, optB, BLANK_VOTE]);
    chain.disconnect();
});

test("fetch_vote_multiple_spends_ok", async () => {
    // TODO: Add test with multiple spends at different height.
    // Test that the first one counts.
});

test("fetch_vote_multiple_spends_error", async () => {
    // TODO: Add test with multiple spends at same height,
    // verify that this spoils the vote.
});

test("tally_votes", async () => {
    jest.setTimeout(30000);
    const vote = await get_example_vote();

    const chain = new Blockchain();
    await chain.connect();

    const result = await tally_votes(chain, vote.proposal, vote.optA, vote.optB,
        vote.voters, vote.endheight, true);

    const OPT_A = 0;
    const OPT_B = 1;
    const BLANK = 2;
    const NO_VOTE = 3;
    const INVALID_VOTE = 4;

    expect(result[OPT_A].length).toBe(1);
    expect(result[OPT_B].length).toBe(1);
    expect(result[BLANK].length).toBe(2);
    expect(result[NO_VOTE].length).toBe(1);
    expect(result[INVALID_VOTE].length).toBe(1);

    expect(result[OPT_A][0][1]).toBe(vote.voters[0]);
    expect(result[OPT_B][0][1]).toBe(vote.voters[1]);
    expect(result[BLANK][0][1]).toBe(vote.voters[2]);
    expect(result[BLANK][1][1]).toBe(vote.voters[3]);
    expect(result[INVALID_VOTE][0]).toBe(vote.voters[4]);
    expect(result[NO_VOTE][0]).toBe(vote.voters[5]);

    chain.disconnect();
});
