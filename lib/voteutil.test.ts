import { generate_private_key, get_example_vote } from "./testutil";
import { derive_contract_adr, hash_to_address, create_vote_message, sign_vote_message, hash160_salted, two_option_contract_redeemscript } from "./voteutil";

test("hash_to_address", () => {
    expect(hash_to_address(Buffer.from("ece079fd8364c0db3c202560110d06063a6152c1", "hex"), "p2sh"))
        .toEqual("bitcoincash:prkwq70asdjvpkeuyqjkqygdqcrr5c2jcy9aav5fpz");
    expect(hash_to_address(Buffer.from("09cbf1886934f0f25e3133fb7d4c34848fb2d41a", "hex"), "p2pkh"))
        .toEqual("bitcoincash:qqyuhuvgdy60puj7xyelkl2vxjzglvk5rg9elmke8u");
});

test("derive_contract_adr", async () => {

    const vote = await get_example_vote();
    expect(await derive_contract_adr(
        vote.proposal, vote.optA, vote.optB, vote.voters[0]))
        .toEqual("bitcoincash:prx7fmgt7rav309qxjnxdw6zhran36t9dcvpyhsw4j");

    expect(await derive_contract_adr(
        vote.proposal, vote.optA, vote.optB, vote.voters[1]))
        .toEqual("bitcoincash:pqj48avrnj5wzshf9ye7rqu5m3v3wj3cj5439m3w0d");

    expect(await derive_contract_adr(
        vote.proposal, vote.optA, vote.optB, vote.voters[2]))
        .toEqual("bitcoincash:pzrpg8yrvrwsqkk8rzwu4mjncmcxgt0spv2pjfxcfs");
});

test("create_vote_message", async () => {
    const salt = Buffer.from("unittest");
    const id = await hash160_salted(salt, Buffer.from("Proposal ID"));
    const vote = await hash160_salted(salt, Buffer.from("Vote option"));

    const message = create_vote_message(id, vote);
    expect(message.length).toBe(40);
    expect(message.subarray(0, 20)).toStrictEqual(id);
    expect(message.subarray(20)).toStrictEqual(vote);
});

test("sign_vote_message", async () => {
    const salt = Buffer.from("unittest");
    const id = await hash160_salted(salt, Buffer.from("Proposal ID"));
    const vote = await hash160_salted(salt, Buffer.from("Vote option"));
    const message = create_vote_message(id, vote);

    const privatekey = generate_private_key()
    const signature = await sign_vote_message(privatekey, message)

    // This framework produces Schnorr signature
    expect(signature.length).toBe(64)
});

test("test_two_option_contract_redeemscript", async () => {
    const vote = await get_example_vote();
    const redeemScript = two_option_contract_redeemscript(
        vote.proposal,
        vote.optA,
        vote.optB,
        vote.voters[0]);
    expect(redeemScript.toString("hex")).toBe("14485beda9a544ee01effccef8d2be1e2778f5997e14a7557a06649059c24c7f8b13b955a6550444aa24147563cd871bc9af8f5bcd89a298f94d03135185f5140e2f5d8a017c4983cb56320d18f66bf01587aa445479a988547a5479ad557a5579557abb537901147f75537a887b01147f77767b8778537a879b7c14beefffffffffffffffffffffffffffffffffffff879b");
});
