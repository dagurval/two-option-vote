import { calc_proposal_id, hash160_salted } from "./voteutil";
const assert = require("assert");
import { encodeCashAddress, CashAddressNetworkPrefix, CashAddressType,
    generatePrivateKey, instantiateSecp256k1 } from "@bitauth/libauth"
import { randomBytes } from "crypto";

export function generate_private_key() : Uint8Array {
    return generatePrivateKey(() => randomBytes(32));
}

export function create_random_addr(): string {
    return encodeCashAddress(CashAddressNetworkPrefix.mainnet, CashAddressType.P2PKH,
                             generate_private_key())
}

/**
 * This example vote is based on the `bin/vote-test.ts` test run example.
 *
 * A vote with the default salt and endheight exist on the BCH blockchain and
 * was created by running that example script.
 */
export async function get_example_vote(
    salt: Buffer = Buffer.from("unittest"),
    endheight: number = 642042) {
    const votersPKH: Buffer[] = [
        Buffer.from("0e2f5d8a017c4983cb56320d18f66bf01587aa44", "hex"),
        Buffer.from("3790bb07029831ec90f8eb1ed7c1c09aac178185", "hex"),
        Buffer.from("aaa8a14f0658c44809580b24299a592d7623976c", "hex"),
        Buffer.from("9b1772d9287b9a3587b0a1e147f34714697c780c", "hex"),
        Buffer.from("ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004", "hex"),
        Buffer.from("f7d1358c4baa20b31e5f84c238e964473d733f75", "hex"),
    ];

    const proposalID: Buffer = await calc_proposal_id(salt,
        "Foo?", "Bar", "Baz", endheight, votersPKH);
    const optA = await hash160_salted(salt, Buffer.from("Bar"));
    const optB = await hash160_salted(salt, Buffer.from("Baz"));

    if (salt.compare(Buffer.from("unittest")) === 0) {
        assert(proposalID.toString("hex") === "485beda9a544ee01effccef8d2be1e2778f5997e");
        assert(optA.toString("hex") === "7563cd871bc9af8f5bcd89a298f94d03135185f5");
        assert(optB.toString("hex") === "a7557a06649059c24c7f8b13b955a6550444aa24");
    }

    return {
        endheight,
        optA,
        optB,
        proposal: proposalID,
        voters: votersPKH,
    };
}
