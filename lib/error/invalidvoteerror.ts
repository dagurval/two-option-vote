export class InvalidVoteError extends Error {
    constructor(message: string) {
        super(message);
    }
}
