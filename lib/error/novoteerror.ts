import { InvalidVoteError } from "./invalidvoteerror";

export class NoVoteError extends InvalidVoteError {
    constructor() {
        super("No vote cast");
    }
}
