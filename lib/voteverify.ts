import { Blockchain } from "./blockchain";
import { InvalidVoteError } from "./error/invalidvoteerror";
import { NoVoteError } from "./error/novoteerror";
import { OP_DATA_40, OP_DATA_20, OP_DATA_33, OP_DATA_65, OP_PUSHDATA1 } from "./script";
import { BLANK_VOTE } from "./votedef";
import { derive_contract_adr, hash160,
     hash_to_address, two_option_contract_redeemscript } from "./voteutil";

const util = require("util");
const assert = require("assert");

function throw_unless_opcode(script: Buffer, i: number, want: number) {
    if (script[i] === want) {
        return;
    }
    throw new InvalidVoteError(util.format(
        "Expected opcode %d at position %d, got %d",
        want, i, script[i]));
}

/// Fetches a potential vote tx from the blockchain, but does not
/// verify that it's actually a voting transaction.
export async function fetch_vote_tx(
    chain: Blockchain, contractAddr: string, endheight: number, includeUnconfirmed: boolean) {

    const txs = await chain.get_spending_txs(
        contractAddr, endheight, includeUnconfirmed);

    if (txs.length === 0) {
        throw new NoVoteError();
    }
    if (txs.length > 1) {
        throw new InvalidVoteError("Multiple spends");
    }
    return txs[0];
}

function is_plausible_signature_size(signatureSize: number) {
    return signatureSize >= 64 && signatureSize <= 73;
}

/***
 * Gets the vote option casted in a transaction.
 *
 * Verifies that the transaction is a two-option-vote contract with given
 * proposal id, options and belongs to given voter.
 */
export function get_verified_vote(tx: any,
                                  proposalID: Buffer,
                                  optA: Buffer,
                                  optB: Buffer,
                                  voterPKH: Buffer) {

    if (tx.inputs.length > 1) {
        throw new InvalidVoteError("Multiple inputs NYI");
    }

    const script = tx.inputs[0].unlockingBytecode;
    let pos = 0;

    // push msg signature size
    const msgSigSize = script[pos++]
    if (!is_plausible_signature_size(msgSigSize)) {
        throw new InvalidVoteError(util.format(
            "Unexpected message signature size (%d)", msgSigSize));
    }
    const msgSig = script.slice(pos, pos + msgSigSize)
    pos += msgSigSize

    // message
    throw_unless_opcode(script, pos++, OP_DATA_40);
    const msg = script.slice(pos, pos += OP_DATA_40)

    const id = msg.slice(0, 20)
    const option = msg.slice(20)

    if (id.compare(proposalID) !== 0) {
        throw new InvalidVoteError(util.format(
            "Wrong proposal ID, expected '%s', got '%s'",
            proposalID, id));
    }

    if (option.compare(optA) !== 0
        && option.compare(optB) !== 0
        && option.compare(BLANK_VOTE) !== 0) {
        throw new InvalidVoteError(util.format(
            "Invalid vote option '%s'", option.toString("hex")));
    }

    // push tx signature size
    const txSigSize = script[pos++]
    // tx signature
    if (!is_plausible_signature_size(msgSigSize)) {
        throw new InvalidVoteError(util.format(
            "Unexpected tx signature size (%d)", txSigSize));
    }
    const signature = script.slice(pos, txSigSize);
    pos += txSigSize;

    // push 33 - <compressed public key>
    throw_unless_opcode(script, pos++, OP_DATA_33);
    const pubkey = script.slice(pos, pos += OP_DATA_33);

    // OP_PUSHDATA1
    throw_unless_opcode(script, pos++, OP_PUSHDATA1);
    pos++; // skip redeemScript length

    const redeemScript = two_option_contract_redeemscript(
        proposalID, optA, optB, voterPKH);

    // the rest of the scriptSig should be the reedemScript
    if (redeemScript.compare(script.slice(pos)) !== 0) {
        throw new InvalidVoteError(util.format(
            "Wrong redeem script, expected '%s', got '%s'",
            redeemScript.toString("hex"),
            script.slice(pos).toString("hex")));
    }

    return option;
}

export async function tally_votes(
    chain: Blockchain,
    proposalID: Buffer,
    optA: Buffer, optB: Buffer,
    voters: Buffer[], endheight: number, includeUnconfirmed: boolean) {
    const optionA: [any, Buffer][] = [];
    const optionB: [any, Buffer][] = [];
    const blank: [any, Buffer][] = [];

    const noVotes: Buffer[] = [];
    const invalidVotes: Buffer[] = [];

    for (const v of voters) {
        const addr = await derive_contract_adr(proposalID, optA, optB, v);
        try {
            const tx = await fetch_vote_tx(chain, addr, endheight, includeUnconfirmed);
            const opt = get_verified_vote(tx, proposalID, optA, optB, v);
            if (opt.compare(optA) === 0) {
                optionA.push([tx, v]);
            } else if (opt.compare(optB) === 0) {
                optionB.push([tx, v]);
            } else if (opt.compare(BLANK_VOTE) === 0) {
                blank.push([tx, v]);
            } else {
                assert(false);
            }
        } catch (e) {
            if (e instanceof NoVoteError) {
                noVotes.push(v);
            } else if (e instanceof InvalidVoteError) {
                invalidVotes.push(v);
            } else {
                throw e;
            }
        }
    }

    return [optionA, optionB, blank, noVotes, invalidVotes];
}

