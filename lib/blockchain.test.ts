import { Blockchain } from "./blockchain";
import { get_txid } from "./voteutil";

test("get_spending_txs", async () => {
    jest.setTimeout(30000);
    expect.assertions(4);
    const chain = new Blockchain();
    await chain.connect();

    const res = await chain.get_spending_txs(
        "bitcoincash:qrfat0epsxsvq0tgdve74r9tuu5cqhx0hq9y9vzsva", 623176, false);

    expect(res.length).toEqual(3)

    const ids = [];
    for (const tx of res) {
        ids.push(await get_txid(tx));
    }
    ids.sort();

    expect(ids[0]).toEqual("12c99127e82856c9c3e8532857e49e38e0e1a0d1f1a7bd988dc1e8749d3094d4")
    expect(ids[1]).toEqual("447ff6da6ced10ba036396779550b2a7843aabee6c4a2a562090070da79d7230")
    expect(ids[2]).toEqual("60caadfa183c63192dd8806c7cfc3918bce13c58cfb104e010c6623b36b26a17")

    chain.disconnect();
});

test("get_spending_txs_heightfilter", async () => {
    jest.setTimeout(30000);
    expect.assertions(3);
    const chain = new Blockchain();
    await chain.connect();

    const addr = "bitcoincash:qrskglyrvfk9jz6hcnaxcm8852eskzazhgq4r0mf8g";
    const confirmHeight = 624858;
    let res = await chain.get_spending_txs(addr, confirmHeight - 1, true);
    expect(0).toEqual(res.length);
    res = await chain.get_spending_txs(addr, confirmHeight, true);
    expect(2).toEqual(res.length);
    res = await chain.get_spending_txs(addr, confirmHeight + 1, true);
    expect(2).toEqual(res.length);
    chain.disconnect();
});
